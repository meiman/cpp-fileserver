Vaja on vähemalt Qt 5.10 ja c++ 11

Kompileerimine:
	Tee kõike nii serveri kui kliendiga.
	Liigu kausta kus on .pro laiendiga fail.
	Kirjuta 2 käsku:
	"qmake"
	"make"
	Kui OS on windows ja minGW olemas, siis "make" asemel "mingw32-make".
	
Käivitamine:
	Alamkausta bin tekib fail mida saab käivitada.
	Programmile saab anda käsurealt täpselt 2 argumenti: ip ja port. Neid ei kontrollita, eeldan korrektset kasutamist.
	Kui argumente ei anna, siis on ip localhost ja port 12310.
	
Server
	Algselt pole serveris ühtegi kasutajat, enne kliendiga ühendumist peab vähemalt ühe kasutaja looma, muidu ei saa sisse logida.
	Server tekitab faili users.txt kui hoitakse kasutajate andmeid.
	Igale kasutajale luuakse failide jaoks kaust, mille nimi on sama, mis kasutaja nimi.
	Kõik loodud failid ja kuastad pannakse jooksvasse kausta (see kasut kust rakendus tööle pannakse).
	
Klient
	Klient saab valida ip ja pordi, vaikimisi on valitud localhost ja 12310 port.
	Ühendumisel peab klient sisselogima.
	Klient ei saa uut kasutajat luua, seda peab tegema serveris.
	Igal kasutajal on oma failid, teiste kasutajate faile ei näe.
