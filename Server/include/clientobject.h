#ifndef CLIENTOBJECT_H
#define CLIENTOBJECT_H

#include <QtCore>
#include <QtNetwork/QHostAddress>

class ClientObject  {
public:
    ClientObject(){}

    ClientObject(QString name, QString pass, QString dir)  {
        this->name = name;
        this->password = pass;
        this->dirName = dir;
    }

    ClientObject(const ClientObject& obj)   {
        this->name = obj.name;
        this->addr = obj.addr;
        this->port = obj.port;
        this->password = obj.password;
        this->dirName = obj.dirName;
    }

    inline QString getName()    {
        return name;
    }

    inline QHostAddress getAddr()   {
        return addr;
    }

    inline quint16 getPort()    {
        return port;
    }

    inline void setName(QString n)   {
        name = n;
    }

    inline void setPassword(QString pas)   {
        password = pas;
    }

    inline void setDirName(QString dirName) {
        this->dirName = dirName;
    }

    inline QString getDirName() {
        return dirName;
    }

    inline bool auth(QString un, QString pas)  {
        return QString::compare(un, name) == 0 && QString::compare(pas, password) == 0;
    }

    inline QString toString()   {
        QString out = "Client ";
        out.append(name).append(" ").append(addr.toString()).append(":").append(port);
        return out;
    }

    inline friend operator ==(ClientObject o1, ClientObject o2) {
        return (o1.getName() == o2.getName()
                &&o1.getAddr() == o2.getAddr()
                &&o1.getPort() == o2.getPort());
    }

private:
    QString name;
    QHostAddress addr;
    quint16 port;
    QString password;
    QString dirName;
};


#endif // CLIENTOBJECT_H
