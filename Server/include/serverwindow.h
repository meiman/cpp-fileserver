#ifndef SERVERWINDOW_H
#define SERVERWINDOW_H

#include <QMainWindow>
#include "clientobject.h"

class QTcpServer;
class QLabel;
class QListWidget;
class QTcpSocket;
class QLineEdit;
class QPushButton;

class ServerWindow : public QMainWindow
{
    Q_OBJECT

public:
    ServerWindow(QWidget *parent = 0);
    void startServer(QString ip, quint16 port);
    ~ServerWindow();

private slots:
    void initConnection();
    void receiveCommand();

private:
    void openServer(QString ip, quint16 port);
    void initStatusBar();
    void initUI();
    void closeConnection();
    void receiveFile(QString fileName, QByteArray fileData);
    void sendToClient(QString message, QByteArray data);
    void changeUserDirectory();
    void sendFileList();
    void readUserFilesList();
    void sendFile(QString filePath);
    void deleteFile(QString fileName);
    void readUsers();
    void createUser(QString name, QString pass);
    void addUser();

    // UI
    QStatusBar *statusBar = nullptr;
    QLabel *statusLabel = nullptr;
    QListWidget *clientList = nullptr;
    QLabel *addUserLabel = nullptr;
    QLineEdit *usernameEntry = nullptr;
    QLineEdit *passwordEntry = nullptr;
    QPushButton *addUserButton = nullptr;
    // Connection
    QTcpServer *server = nullptr;
    QTcpSocket *client = nullptr;
    QString serverIP;
    quint16 serverPort;
    QDataStream inStream;
    qint64 blockSize=0;
    // Clients
    ClientObject activeClient;
    QFileInfoList userFiles;
    QDir userHomeDir;
    QList<ClientObject> clients;
    QFile usersFile {"./users.txt"};
};

#endif // SERVERWINDOW_H
