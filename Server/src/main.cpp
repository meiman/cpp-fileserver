#include "../include/serverwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QStringList args = a.arguments();
    QString ip = "127.0.0.1";
    quint16 port = 12310;

    if (args.count() == 3)  {
        // port and ip
        ip = args.at(1);
        QString portString = args.at(2);
        port = portString.toUInt();
    } else if (args.count() != 1)   {
        qDebug() << "Invalid arguments";
        return 0;
    }

    qDebug() << "ip: " << ip << " port: " << port;

    ServerWindow w;
    w.setFixedSize(400, 200);
    w.startServer(ip, port);
    w.show();

    return a.exec();
}
