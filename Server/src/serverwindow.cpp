#include <QtWidgets>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtCore>
#include <QtNetwork/QHostAddress>
#include <QVBoxLayout>
#include <QDebug>

#include "../include/serverwindow.h"

ServerWindow::ServerWindow(QWidget *parent)
    : QMainWindow(parent),
      statusLabel(new QLabel)
{
}

ServerWindow::~ServerWindow()
{
    server->close();
    delete server;
}

void ServerWindow::startServer(QString ip, quint16 port)    {
    openServer(ip, port);
    readUsers();
    initUI();
    initStatusBar();

    connect(server, &QTcpServer::newConnection, this, &ServerWindow::initConnection);
    connect(addUserButton, &QAbstractButton::clicked, this, &ServerWindow::addUser);
    statusLabel->setText("Waiting for connection");
}

void ServerWindow::initConnection()   {
    statusLabel->setText("Connecting");
    client = server->nextPendingConnection();

    connect(client, &QAbstractSocket::disconnected, client, &QObject::deleteLater); // ensure connection is closed
    connect(client, &QAbstractSocket::disconnected, this, &ServerWindow::closeConnection);
    connect(client, &QTcpSocket::readyRead, this, &ServerWindow::receiveCommand);
    userFiles.clear();
    sendToClient("HELLO", QByteArray());
}

void ServerWindow::addUser()    {
    QString username = usernameEntry->text();

    bool nameExists = false;
    foreach (ClientObject c, clients) {
        if (QString::compare(c.getName(), username) == 0)    {
            nameExists = true;
            break;
        }
    }

    if (username.isEmpty() || nameExists) {
        QMessageBox::warning(this, tr("Add user"),
                                      tr("Unable to add user, username field empty or username already exists"));
        return;
    }

    QString password = QCryptographicHash::hash(passwordEntry->text().toStdString().c_str(), QCryptographicHash::Sha256).toBase64();

    if (!usersFile.open(QIODevice::ReadWrite | QIODevice::Text))
            return;


    QTextStream toFile(&usersFile);
    QString line = username+":"+password;
    usersFile.seek(usersFile.size());
    toFile << line << '\n';
    usersFile.close();

    usernameEntry->clear();
    passwordEntry->clear();
}

void ServerWindow::readUsers()  {
    qDebug() << "Reading users";
    if (!usersFile.open(QIODevice::ReadWrite | QIODevice::Text))
            return;

    QTextStream fromFile(&usersFile);
    QString line;
    while (fromFile.readLineInto(&line))    {
        clients.append(ClientObject(line.section(":",0,0), line.section(":",1), line.section(":",0,0)));
        qDebug() << "User: " << line.section(":",0,0) << " Password: " << line.section(":", 1);
    }
    usersFile.close();
}

void ServerWindow::changeUserDirectory()    {
    userHomeDir = QDir(activeClient.getDirName());
    if (!userHomeDir.exists())  {
        qDebug() << "Creating directory" ;
        QDir::current().mkdir(activeClient.getDirName());
    }

    qDebug() << "User home directory: " << userHomeDir.path();

    readUserFilesList();
}

void ServerWindow::readUserFilesList()  {
    qDebug() << "Reading dir content" ;

    userHomeDir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    userFiles.clear();
    userFiles = userHomeDir.entryInfoList();
    for (int i = 0; i < userFiles.size(); i++)  {
        QFileInfo fileInfo = userFiles.at(i);
        qDebug() << "File: " << fileInfo.fileName();
    }
}

void ServerWindow::receiveCommand() {
    QDataStream in(client);
    in.setVersion(QDataStream::Qt_4_0);

    qDebug() << "Receiving, available: " << client->bytesAvailable();
    client->flush();
    if (blockSize == 0) {
            if (client->bytesAvailable() < sizeof(quint64)) {
                qDebug() << "Less than 4 bytes";
                return;
            }
            qDebug() << "Reading block size";
            in >> blockSize;
    }
    qDebug() << "Block size: " << blockSize;
    if (client->bytesAvailable() < blockSize)   {
        qDebug() << "Less data than block size";
        return;
    }

    QString message;
    in >> message;

    qDebug() << "Got message: " << message;
    QString command = message.section(":",0,0);

    if (QString::compare(command, "CLOSE") == 0)   {
        //closeConnection();
    } else if (QString::compare(command, "SENDF") == 0) {
        QString newFileName = message.section(":",1);
        QByteArray newFileData = client->readAll();
        qDebug() << "File data: " << newFileData.size();
        receiveFile(newFileName, newFileData);
    } else if (QString::compare(command, "FILES") == 0) {
        sendFileList();
    } else if (QString::compare(command, "DWNL") == 0) {
        QString requestFileName = message.section(":",1);
        sendFile(requestFileName);
    } else if (QString::compare(command, "DEL") == 0)    {
        QString deleteFileName = message.section(":",1);
        deleteFile(deleteFileName);
    } else if (QString::compare(command, "AUTH") == 0)    {
        QString username = message.section(":", 1);
        QString password = client->readAll().toBase64();
        qDebug() << "Username: " << username;
        qDebug() << "Password: " << password;
        foreach (ClientObject c, clients) {
            if (c.auth(username, password)) {
                qDebug() << "Auth successful";
                new QListWidgetItem(tr("Client: %1 at %2:%3")
                                    .arg(username)
                                    .arg(client->peerAddress().toString())
                                    .arg(client->peerPort()), clientList);

                statusLabel->setText("Connected to client");
                activeClient = c;
                changeUserDirectory();
                sendToClient("AUTH:true", QByteArray());
                blockSize = 0;
                return;
            }
        }
        qDebug() << "Auth failed";
        sendToClient("AUTH:false", QByteArray());
    } else  {
        qDebug() << "Unknown command";
    }
    blockSize = 0;
}

void ServerWindow::sendToClient(QString message, QByteArray data)    {
    QByteArray block;
    QDataStream outStream(&block, QIODevice::WriteOnly);
    outStream.setVersion(QDataStream::Qt_4_0);

    outStream << (quint64)0;    // Room for the block size
    outStream << message;       // Command and arguments
    if (!data.isEmpty()) block.append(data); // Data
    outStream.device()->seek(0);    // Back to start
    outStream << (quint64)(block.size() - sizeof(quint64)); // Data size

    quint64 bytesWritten = client->write(block);
    client->waitForBytesWritten();

    qDebug() << "Sent message: " << message << " bytes sent: " << bytesWritten;
}

void ServerWindow::sendFile(QString filePath)   {
    // Open file
    QFile file(userHomeDir.path() + "/" + filePath);
    if (!file.open(QIODevice::ReadOnly))  return;

    qDebug() << "File size: " << file.size();

    sendToClient("SENDF:"+filePath.section("/", -1), file.readAll());

    file.close();

    qDebug() << "Sent file: " << filePath;
}

void ServerWindow::sendFileList()   {
    QString message = "LIST:";
    for (int i =0; i < userFiles.size(); i++)   {
        qDebug() << tr("Sending file name: %1").arg(userFiles.at(i).fileName());
        message.append(userFiles.at(i).fileName());
        if (i!=userFiles.size()-1)  message.append("/");
    }
    sendToClient(message, QByteArray());
}

void ServerWindow::deleteFile(QString fileName) {
    // Open file
    QFile file(userHomeDir.path() + "/" + fileName);
    if (!file.exists()) return;
    qDebug() << "Deleting file: " << fileName;
    file.remove();
    readUserFilesList();
}

void ServerWindow::closeConnection()  {
    statusLabel->setText("Client disconnected");
    for (int i=0; i<clientList->count(); i++)    {
        if (clientList->item(i)->text().contains(activeClient.getName()))   {
            delete clientList->item(i);
            break;
        }
    }
}

void ServerWindow::receiveFile(QString fileName, QByteArray fileData)    {
    // Save file
    QString path = userHomeDir.path();
    qDebug() << "fileName: " << fileName;
    qDebug() << "targetFile: " << path+"/"+fileName;
    QFile targetFile(path+"/"+fileName);

    if (!targetFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Can't open file";
        return;
    }
    targetFile.write(fileData);

    targetFile.close();
    qDebug() << "Saved file: " << fileName;
    readUserFilesList();
}

void ServerWindow::openServer(QString ip, quint16 port) {
    statusLabel->setText("The server is starting");
    server = new QTcpServer(this);
    qint32 ipAddress = QHostAddress(ip).toIPv4Address();
    serverIP = QHostAddress(ipAddress).toString();
    serverPort = port;
    if (!server->listen(QHostAddress(ipAddress), serverPort))  {
        QMessageBox::critical(this, tr("File Server"),
                                      tr("Unable to start the server: %1.")
                                      .arg(server->errorString()));
        close();
        return;
    }
}

void ServerWindow::initUI()   {
    QLabel *serverAddressLabel = new QLabel(this);
    serverAddressLabel->setText(tr("The server is running on IP: %1 port: %2").arg(serverIP).arg(serverPort));

    clientList = new QListWidget(this);

    addUserLabel = new QLabel("Add user", this);
    addUserButton = new QPushButton("Add user", this);
    usernameEntry = new QLineEdit(this);
    usernameEntry->setPlaceholderText("Username");
    passwordEntry = new QLineEdit(this);
    passwordEntry->setPlaceholderText("Password");
    passwordEntry->setEchoMode(QLineEdit::Password);

    QGridLayout *grid = new QGridLayout(this);
    grid->addWidget(addUserLabel, 0, 0);
    grid->addWidget(usernameEntry, 1, 0);
    grid->addWidget(passwordEntry, 1, 1);
    grid->addWidget(addUserButton, 1, 2);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(serverAddressLabel);
    layout->addWidget(clientList);
    layout->addLayout(grid);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(layout);

    this->setCentralWidget(centralWidget);
}

void ServerWindow::initStatusBar()    {
    statusBar = new QStatusBar(this);
    statusBar->addPermanentWidget(statusLabel);
    setStatusBar(statusBar);
}


