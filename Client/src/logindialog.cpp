#include "../include/logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) : QDialog(parent)
{
    setupUI();
    setWindowTitle("Login");
    setModal(true); // Blocks other windows
}

void LoginDialog::setupUI() {
    usernameLabel = new QLabel("Username", this);
    passwordLabel = new QLabel("Password", this);
    usernameEdit = new QLineEdit(this);
    passwordEdit = new QLineEdit(this);
    passwordEdit->setEchoMode(QLineEdit::Password);
    usernameLabel->setBuddy(usernameEdit);
    passwordLabel->setBuddy(passwordEdit);

    loginButton = new QPushButton("Login", this);
    cancelButton = new QPushButton("Cancel", this);

    connect(loginButton, &QAbstractButton::clicked, this, &LoginDialog::login);
    connect(cancelButton, &QAbstractButton::clicked, this, &LoginDialog::close);

    QGridLayout *layout = new QGridLayout(this);

    layout->addWidget(usernameLabel, 0, 0);
    layout->addWidget(usernameEdit, 0, 1);
    layout->addWidget(passwordLabel, 1, 0);
    layout->addWidget(passwordEdit, 1, 1);
    layout->addWidget(loginButton, 2, 0);
    layout->addWidget(cancelButton, 2, 1);

    setLayout(layout);
}

void LoginDialog::login()   {
    QString username = usernameEdit->text();
    QByteArray password = QCryptographicHash::hash(passwordEdit->text().toStdString().c_str(), QCryptographicHash::Sha256);

    emit getLogin(username, password);

    close();
}



