#include <QtWidgets>
#include <QtNetwork/QTcpServer>
#include <QtCore>
#include <QtDebug>

#include "../include/clientwindow.h"

ClientWindow::ClientWindow(QWidget *parent)
    : QMainWindow(parent),
      socket(new QTcpSocket(this))
{
    // create UI elements
    initUI();

    // Connect event
    connect(connectButton, &QAbstractButton::clicked, this, &ClientWindow::login);
    // Disconnect event
    connect(disconnectButton, &QAbstractButton::clicked, this, &ClientWindow::disconnectFromServer);
    // Data available event
    connect(socket, &QIODevice::readyRead, this, &ClientWindow::receiveFromServer);
    // Error event
    connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &ClientWindow::displayError);
    // File selection
    connect(uploadFileButton, &QAbstractButton::clicked, this, &ClientWindow::openFileBrowser);
    // Refresh file list
    connect(refreshFileListButton, &QAbstractButton::clicked, this, &ClientWindow::refreshFileList);
    // Download file
    connect(downloadFileButton, &QAbstractButton::clicked, this, &ClientWindow::downloadFile);
    // Delete file from server
    connect(deleteFileButton, &QAbstractButton::clicked, this, &ClientWindow::deleteFile);
}

ClientWindow::~ClientWindow()
{
    delete socket;
}

void ClientWindow::connectToServer(QString& username, QByteArray& password)    {
    qDebug() << "Username: " << username;
    qDebug() << "Password: " << password.toBase64();
    this->username = username;
    this->password = password;
    loggedin = false;
    fileList->clear();
    socket->abort(); //close previous connection if it exists
    socket->connectToHost(ipEntry->text(), portEntry->text().toInt()); //try to connect, emits readReady or error
}

void ClientWindow::disconnectFromServer()   {
    if (socket->state() == QAbstractSocket::ConnectedState) {
        sendToServer("CLOSE", QByteArray());
        socket->disconnectFromHost();
        fileList->clear();
        loggedin = false;
        connectButton->setEnabled(true);
        disconnectButton->setEnabled(false);
        uploadFileButton->setEnabled(false);
        downloadFileButton->setEnabled(false);
        refreshFileListButton->setEnabled(false);
        deleteFileButton->setEnabled(false);
    }
}

void ClientWindow::login()  {
    loginDialog = new LoginDialog(this);
    connect(loginDialog, SIGNAL(getLogin(QString&, QByteArray&)), this, SLOT(connectToServer(QString&, QByteArray&)));
    loginDialog->exec();
}

void ClientWindow::refreshFileList()    {
    sendToServer("FILES", QByteArray());
}

void ClientWindow::openFileBrowser()    {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select file"), "C:\\", "All files (*.*)");
    qDebug() << "File selected: " << fileName;

    if (!fileName.isEmpty())    {
        sendFile(fileName);
    }
}

void ClientWindow::sendFile(QString filePath)   {
    // Open file
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly))  return;

    qDebug() << "File size: " << file.size();

    sendToServer("SENDF:"+filePath.section("/", -1), file.readAll());

    file.close();

    qDebug() << "Sent file: " << filePath;
    refreshFileList();
}

void ClientWindow::downloadFile()   {
    if (fileList->currentItem())    {
        QString fileName = fileList->currentItem()->text();
        qDebug() << "Downloading file: " << fileName;
        sendToServer("DWNL:"+fileName, QByteArray());
    } else  {
        qDebug() << "No file selected";
    }
}

void ClientWindow::deleteFile() {
    if (fileList->currentItem())    {
        QString fileName = fileList->currentItem()->text();
        qDebug() << "Deleting file: " << fileName;
        sendToServer("DEL:"+fileName, QByteArray());
        removeFileFromList(fileName);
        refreshFileList();
    } else  {
        qDebug() << "No file selected";
    }
}

void ClientWindow::receiveFromServer()  {
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_4_0);

    if (blockSize == 0) {
        // Not enough data in stream
        if (socket->bytesAvailable() < sizeof(quint64)) return;
        // Save blocksize
        in >> blockSize;
    }
    // Not enough data in stream
    if (socket->bytesAvailable() < blockSize)   return;

    QString message;
    in >> message;

    qDebug() << "Got message: " << message;
    QString command = message.section(":",0,0);

    if (QString::compare(command, "HELLO") == 0)    {
        if (!loggedin)  {
            qDebug() << "Send login creadentials";
            sendToServer("AUTH:"+username, password);
        }
        else {
            refreshFileList();
        }
    } else if (QString::compare(command, "AUTH") == 0) {
        QString isAuth = message.section(":", 1);
        if (QString::compare(isAuth, "true") == 0) {
            qDebug() << "Authenticated";
            loggedin = true;
            connectButton->setEnabled(false);
            disconnectButton->setEnabled(true);
            uploadFileButton->setEnabled(true);
            downloadFileButton->setEnabled(true);
            refreshFileListButton->setEnabled(true);
            deleteFileButton->setEnabled(true);
            refreshFileList();
        }   else    {
            qDebug() << "Invalid login";
            QMessageBox::warning(this, tr("Login failed"),
                                     tr("Authentication failed, invalid username or password."));
            disconnectFromServer();
        }

    } else if (QString::compare(command, "LIST") == 0)  {
        QString filesAsString = message.section(":",1);
        updateFileList(filesAsString);
    } else if (QString::compare(command, "SENDF") == 0) {
        QString newFileName = message.section(":",1);
        QByteArray newFileData = socket->readAll();
        qDebug() << "File data: " << newFileData.size();
        receiveFile(newFileName, newFileData);
    } else  {
        qDebug() << "Unknown command";
    }
    blockSize = 0;
}

void ClientWindow::receiveFile(QString fileName, QByteArray fileData)    {
    // Save file
    QDir userHomeDir = QDir("./user");
    if (!userHomeDir.exists())  {
        qDebug() << "Creating directory" ;
        QDir::current().mkdir("user");
    }
    QString path = userHomeDir.path();
    qDebug() << "fileName: " << fileName;
    qDebug() << "targetFile: " << path+"/"+fileName;
    QFile targetFile(path+"/"+fileName);

    if (!targetFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Can't open file";
        return;
    }
    targetFile.write(fileData);

    targetFile.close();
    qDebug() << "Saved file: " << fileName;
}

void ClientWindow::updateFileList(QString filesAsString) {
    qDebug() << "File list: " << filesAsString;

    QList<QString> files = filesAsString.split("/");

    for (int i=0; i<files.size(); i++)  {
        addToFileList(files.at(i));
    }

    qDebug() << "All file names updated, server has " << fileList->count() << " files";
}

void ClientWindow::addToFileList(QString fileName)    {
    // Check if list allready contains file
    for (int j=0; j < fileList->count(); j++)   {
        if (QString::compare(fileList->item(j)->data(Qt::DisplayRole).toString(), fileName) == 0)  return;
    }
    new QListWidgetItem(fileName, fileList);
}

void ClientWindow::removeFileFromList(QString fileName) {
    for (int i=0; i < fileList->count(); i++)   {
        if (QString::compare(fileList->item(i)->data(Qt::DisplayRole).toString(), fileName) == 0)   {
            delete fileList->item(i);
            return;
        }
    }
}

void ClientWindow::sendToServer(QString message, QByteArray data)  {
    QByteArray block;
    QDataStream outStream(&block, QIODevice::WriteOnly);
    outStream.setVersion(QDataStream::Qt_4_0);

    qDebug() << "Sending message: " << message;
    qDebug() << "Sending bytes: " << data.size();

    outStream << (quint64)0;    // Room for the block size
    outStream << message;       // Command and arguments
    if (!data.isEmpty()) block.append(data);          // Data
    outStream.device()->seek(0);// Back to start
    outStream << (quint64)(block.size() - sizeof(quint64)); // Data size

    quint64 bytesWritten = socket->write(block);
    socket->waitForBytesWritten();

    qDebug() << "Sent message: " << message << " bytes sent: " << bytesWritten;
}

void ClientWindow::displayError(QAbstractSocket::SocketError socketError)   {
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The host was not found. Please check the "
                                    "ip and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The connection was refused by the peer. "
                                    "Make sure the file server is running, "
                                    "and check that the ip and port "
                                    "settings are correct. %1").arg(socket->error()));
        break;
    default:
        QMessageBox::information(this, tr("Client"),
                                 tr("The following error occurred: %1.")
                                 .arg(socket->errorString()));
    }
}

void ClientWindow::initUI() {
    QLabel *ipLabel = new QLabel(tr("Server IP:"), this);
    QLabel *portLabel = new QLabel(tr("Server port:"), this);

    ipEntry = new QLineEdit("127.0.0.1", this);
    portEntry = new QLineEdit("12310", this);

    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    QRegExp ipRegex ("^" + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange + "$");
    QRegExpValidator *ipv4Validator = new QRegExpValidator(ipRegex, this);

    ipEntry->setValidator(ipv4Validator);
    portEntry->setValidator(new QIntValidator(1, 65535, this));

    ipLabel->setBuddy(ipEntry);
    portLabel->setBuddy(portEntry);

    connectButton = new QPushButton(tr("Connect"), this);
    disconnectButton = new QPushButton(tr("Disconnect"), this);
    disconnectButton->setEnabled(false);

    uploadFileButton = new QPushButton(tr("Upload file"), this);
    uploadFileButton->setEnabled(false);

    downloadFileButton = new QPushButton(tr("Download file"), this);
    downloadFileButton->setEnabled(false);

    deleteFileButton = new QPushButton(tr("Delete file"), this);
    deleteFileButton->setEnabled(false);

    refreshFileListButton = new QPushButton(tr("Refresh file list"), this);
    refreshFileListButton->setEnabled(false);
    fileList = new QListWidget(this);

    QLabel *fileLayoutLabel = new QLabel(tr("Files"), this);
    fileLayoutLabel->setBuddy(refreshFileListButton);

    QVBoxLayout *masterLayout = new QVBoxLayout(this);
    QGridLayout *buttonsLayout = new QGridLayout(this);
    filesLayout = new QGridLayout(this);

    masterLayout->addLayout(buttonsLayout);
    masterLayout->addLayout(filesLayout);

    buttonsLayout->addWidget(ipLabel, 0, 0);
    buttonsLayout->addWidget(ipEntry, 0, 1);
    buttonsLayout->addWidget(portLabel, 1, 0);
    buttonsLayout->addWidget(portEntry, 1, 1);
    buttonsLayout->addWidget(connectButton, 2, 0);
    buttonsLayout->addWidget(disconnectButton, 2, 1);

    filesLayout->addWidget(fileLayoutLabel, 0, 0);
    filesLayout->addWidget(refreshFileListButton, 0, 1);
    filesLayout->addWidget(fileList, 1, 0, 1, 2);
    filesLayout->addWidget(uploadFileButton, 2, 0);
    filesLayout->addWidget(downloadFileButton, 2, 1);
    filesLayout->addWidget(deleteFileButton, 3, 0);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(masterLayout);
    this->setCentralWidget(centralWidget);
}


