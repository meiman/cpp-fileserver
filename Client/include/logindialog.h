#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QtWidgets>
#include <QDialog>

class QLabel;
class QLineEdit;

class LoginDialog : public QDialog
{
    Q_OBJECT
public:
    explicit LoginDialog(QWidget *parent = nullptr);

signals:
    void getLogin(QString& username, QByteArray& password);

private slots:
    void login();

private:
    void setupUI();

    QLabel *usernameLabel = nullptr;
    QLabel *passwordLabel = nullptr;
    QLineEdit *usernameEdit = nullptr;
    QLineEdit *passwordEdit = nullptr;
    QPushButton *loginButton = nullptr;
    QPushButton *cancelButton = nullptr;
};

#endif // LOGINDIALOG_H
