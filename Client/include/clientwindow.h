#ifndef CLIENTWINDOW_H
#define CLIENTWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QDebug>

#include "logindialog.h"

class QTcpSocket;
class QPushButton;
class QLineEdit;
class QLabel;
class QListWidget;
class QGridLayout;

class ClientWindow : public QMainWindow
{
    Q_OBJECT

public:
    ClientWindow(QWidget *parent = 0);
    ~ClientWindow();

private slots:
    // Connection initiation
    void login();           // Open and execute login dialog
    void connectToServer(QString& username, QByteArray& password);
    void disconnectFromServer();
    // Communication, String message:argument, ByteArray data
    void receiveFromServer();
    void sendToServer(QString message, QByteArray data);
    // File operations
    void openFileBrowser(); // Opens file selection dialog and sends selected file
    void refreshFileList(); // Ask file list from server
    void downloadFile();    // Download selected file from server
    void deleteFile();      // Delete file from server
    // Connection errors
    void displayError(QAbstractSocket::SocketError socketError);

private:
    // Create all UI elements
    void initUI();

    // Send file to server, called by openFileBrowser
    void sendFile(QString filePath);
    // Saves received file to disk
    void receiveFile(QString fileName, QByteArray fileData);

    // Updates the filelist ListWidget
    void updateFileList(QString filesAsString);
    // Adds file to filelist ListWidget
    void addToFileList(QString fileName);
    // Removes file from filelist ListWidget
    void removeFileFromList(QString fileName);

    // Communication
    QTcpSocket *socket = nullptr;
    QString serverIP;
    uint serverPort;
    qint64 blockSize = 0;

    // User login
    bool loggedin = false;
    QString username;
    QByteArray password;

    // UI elements
    LoginDialog *loginDialog = nullptr;
    QLineEdit *ipEntry = nullptr;
    QLineEdit *portEntry = nullptr;
    QPushButton *connectButton = nullptr;
    QPushButton *disconnectButton = nullptr;
    QPushButton *uploadFileButton = nullptr;
    QPushButton *downloadFileButton = nullptr;
    QPushButton *refreshFileListButton = nullptr;
    QPushButton *deleteFileButton = nullptr;
    QListWidget *fileList = nullptr;
    QGridLayout *filesLayout = nullptr;
};

#endif // CLIENTWINDOW_H
